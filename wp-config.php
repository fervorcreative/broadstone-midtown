<?php

	// Define Environment Variables
	$environment = new stdClass();
	$environment->local = '/wp-config-local.php';
	$environment->testing = '/wp-config-testing.php';
	$environment->staging = '/wp-config-staging.php';

	// Dynamically Set Environment Constants
	define( 'WP_ENV_LOCAL', file_exists( ABSPATH . $environment->local ) );
	define( 'WP_ENV_TESTING', file_exists( ABSPATH . $environment->testing ) );
	define( 'WP_ENV_STAGING', file_exists( ABSPATH . $environment->staging ) );

	if ( WP_ENV_LOCAL ) {
		require( ABSPATH . $environment->local );

	} elseif ( WP_ENV_TESTING ) {
		require( ABSPATH . $environment->testing );

	} elseif ( WP_ENV_STAGING ) {
		require( ABSPATH . $environment->staging );

	} else {
		define( 'WP_ENV_PRODUCTION', true );
		define( 'DISALLOW_FILE_EDIT', true );
		define( 'DISALLOW_FILE_MODS', true );

		$mysql_hostname = 'localhost';
		$mysql_username = 'bs_midtown7895';
		$mysql_password = ')PmyQe]f;Fm3';
		$mysql_database = 'bs_midtown';
	}

	// Configure MySQL Database Settings
	define( 'DB_USER', $mysql_username );
	define( 'DB_PASSWORD', $mysql_password );
	define( 'DB_NAME', $mysql_database );
	define( 'DB_HOST', $mysql_hostname );

	define( 'DB_CHARSET', 'utf8' );
	define( 'DB_COLLATE', '' );

	$table_prefix = 'wp_';

	// WordPress Security Keys
	// http://codex.wordpress.org/Editing_wp-config.php#Security_Keys
	define( 'AUTH_KEY',         ')P1bvw6=m(}O3kI[%dqOn|Gz5hB@`HLc0LP[?G-mp-W*to zwjs9l}Z%r*S)H=RP' );
	define( 'SECURE_AUTH_KEY',  'WKwc_?B1?6JE:G+./1Xv;p9D7yj^WSDPrw?oVpeaVuz2<*pw~@X@^qGoKx9DT-=L' );
	define( 'LOGGED_IN_KEY',    'NhxPEWoi~VGu[T{O%2~#v=$9`bxKe6YImU-T-Rw?_AJN*3vM3TS%uR~RB/l&56,W' );
	define( 'NONCE_KEY',        '+MDPObCVQW_|PibCS]dU4bu4JHzm[w?ri<jMq4pLd;x.xh^zvx|>,p_KaKN$?/82' );
	define( 'AUTH_SALT',        '?c.q7`&R}7.D6`Fl`&A5oaJ|0AZV_a<d,PC</ZSul<QG[M?gz %Dr+[NE:{EUlL|' );
	define( 'SECURE_AUTH_SALT', '6>Uz$mykxn3o:CT.IOv&wsR$1@%wtBUpW.]G$|:Q+<<-u1w+E0}LFk 5,L):]O9a' );
	define( 'LOGGED_IN_SALT',   'Xx4~t;e)rPb=^fgv4Wx&+Yb-P%;g&}M6D/b:nY;h{SwOLx2m<b&D+ H6t1q$jkkw' );
	define( 'NONCE_SALT',       'f3+;mi@E#aP N4.}PA-zzl1d]7GR(4i74Qb[OYk+CGp|-%>:(mFnpV/BfnP[`^ag' );

	// Use X-Forwarded-For HTTP Header to Get Visitor's Real IP Address
	if ( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
		$http_x_headers = explode( ',', $_SERVER['HTTP_X_FORWARDED_FOR'] );

		$_SERVER['REMOTE_ADDR'] = $http_x_headers[0];
	}

	// Wordpress Application Options + Settings
	// http://codex.wordpress.org/Editing_wp-config.php
	if ( isset( $_SERVER['SERVER_NAME'] ) ) {
		define( 'WP_HOME',	  'http://' . $_SERVER['SERVER_NAME'] );
		define( 'WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] );
	}

	define( 'AUTOSAVE_INTERVAL', 300 );
	define( 'WP_POST_REVISIONS', false );

	define( 'WP_CACHE', false );
	define( 'WP_MEMORY_LIMIT', '128M' );

	if ( ! defined( 'ABSPATH' ) ) {
		define( 'ABSPATH', dirname( __FILE__ ) . '/' );
	}

	require_once( ABSPATH . 'wp-settings.php' );

?>