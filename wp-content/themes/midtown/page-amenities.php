<?php
/**
 * Template Name: Amenities
 *
 */

get_header(); ?>
	
	<section class="page-hero">
		<?php $hero = get_field('page_hero'); ?>
		<img src="<?php echo $hero['url']; ?>" width="100%" height="auto" alt="<?php echo $hero['title']; ?>" />
	</section> 
	
	<section class="page-content amenities-content">
		<div class="row">
			<div class="small-12 medium-8 medium-centered columns page-intro">
				<h2 class="amenities"><?php the_field('page_headline'); ?></h2>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
				<?php endif; ?>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>contact" class="orange-button" id="trigger-1">Contact AN ASSOCIATE</a>
			</div>
		</div>
	</section>

	<section class="amenities-grid">
		<?php if( have_rows('amenities_list') ): ?>
		<ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-5 animate-1">
			<?php while( have_rows('amenities_list') ): the_row(); ?>
			<li>
				<?php $icon = get_sub_field('amenity_icon');
					$amenity = get_sub_field('amenity'); ?>
				<div class="content">
					<div class="content-inner">
						<div class="amenity-icon" style="background-image: url(<?php echo $icon['url']; ?>);"></div>
						<p><?php echo $amenity; ?></p>
					</div>
				</div>
			</li>
			<?php endwhile; ?>
		</ul>
		<?php endif; ?>
	</section>
	
<?php
get_footer();