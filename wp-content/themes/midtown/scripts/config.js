(function($) {
	
	//add classes to form inputs based on their type
	for(var i = 0, len = document.getElementsByTagName('input').length; i<len; i++){
		document.getElementsByTagName('input')[i].className += ' ' + document.getElementsByTagName('input')[i].type;
	}

	//open links in new window (target=_blank); checks if the url is external
	var hostname = window.location.hostname,
	links = document.getElementsByTagName('a'),
	pattern = /^https?:\/\/(www.)?/i;
	 
	for(var i=0, len = links.length; i<len; i++) {
	  if(pattern.test(links[i].href) && links[i].href.toLowerCase().indexOf(hostname) == -1){
	    links[i].target = "_blank";
	    links[i].className += ' external';
	  }
	}

	$(document).ready(function() {
	
		//--------------------------------------- utils ------------------------------------------------//

		// HTML5 placeholders fallback for IE9 and older
	    if ($('html').hasClass('lt-ie10')) {
	 
	        // add the placeholder text as value
	        $('textarea, input:text').each(function() {
	            $(this).attr('value', $(this).attr('placeholder'));
	        });
	 
	        // clear form fields on focus
	        $('textarea, input:text').on('focus click', function(){
	            if (this.value == this.defaultValue) {
	            this.value = '';
	            }
	        }).on('blur', function(){
	            if (this.value == '') {
	            this.value = this.defaultValue;
	            }
	        });
	    }

		//equal height columns function
		function equalHeight(group) {
			var tallest = 0;
			group.each(function() {
				var thisHeight = $(this).height();
				if(thisHeight > tallest) {
					tallest = thisHeight;
				}
			});
			group.height(tallest);
		}
		//equalHeight($("//here put the selector"));

		// custom select
		$('select').each(function(){
			$(this).wrap('<div class="custom-select"></div>');
			$(this).before('<span class="select-placeholder">' + $('option:checked', this).text() + '</span>');

			$(this).on('click touch change', function() {
				$(this).prev('.select-placeholder').empty().html($('option:checked', this).text());
			});
		});

		$('.scroll-to').on('click touch', function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top - $('#header').height()}, 500);
		});
	    
		//---------------------------------- other scripts ---------------------------------------------//

		$(window).scroll(function() {

			if ( $(this).scrollTop() > 0 ) {
				$('body.home').addClass('hasScroll');
			} else {
				$('body.home').removeClass('hasScroll');
			}

		});

		// menu
		$('#nav-toggle').on('click touch', function(event) {
			event.preventDefault();
			$('body').toggleClass('nav-open');
		});

		// gallery
		$('.gallery-caption-toggle').on('click touch', function() {
			$(this).toggleClass('collapsed');
			$(this).parent().next('.slide-captions').slideToggle('fast');
		});

		var $slickElement = $('.gallery .slides');

		$slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
			//currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
			var i = (currentSlide ? currentSlide : 0);
			// $status.text(i + '/' + slick.slideCount);
			$('.gallery-caption .slide-captions .caption').removeClass('active');
			$('.gallery-caption .slide-captions .caption').eq(i).addClass('active');
		});

		$slickElement.slick({
			arrows: true,
			dots: false,
			prevArrow: '<a class="slick-prev slick-arrow"><i class="fa fa-angle-left"></i></a>',
			nextArrow: '<a class="slick-next slick-arrow"><i class="fa fa-angle-right"></i></a>',
		});

		// gallery section
		$('.gallery-section').each(function() {
			var $slidesToShow = 2;
			var $slidesToScroll = 2;

			if ($(this).hasClass('one-slide')) {
				$slidesToShow = 1;
				$slidesToScroll = 1;
			}
			
			$(this).find('.slides').slick({
				infinite: true,
				slidesToShow: $slidesToShow,
				slidesToScroll: $slidesToScroll,
				arrows: true,
				dots: false,
				prevArrow: '<a class="slick-prev slick-arrow"><i class="fa fa-angle-left"></i></a>',
				nextArrow: '<a class="slick-next slick-arrow"><i class="fa fa-angle-right"></i></a>',
				responsive: [
					{
						breakpoint: 740,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}
				],
			});
		});

		// frontpage explore filters
		$('.explore-filters > li > a').on('click touch', function(event) {
			event.preventDefault();
			$(this).closest('li').siblings().removeClass('active');
			$(this).closest('li').addClass('active');
			$('.explore > li').removeClass('active');
			$($(this).attr('href')).addClass('active');

			$('html,body').animate({scrollTop:$($(this).attr('href')).offset().top - $('#header').height()}, 300);
		});


		// tabs
		$('.tabs > li > a').on('click touch', function(event) {
			event.preventDefault();

			$(this).parent().siblings().removeClass('active');
			$(this).parent().addClass('active');

			$(this).closest('.tabs').next('.tabs-wrapper').find('.tab-content').removeClass('active');
			// $(this).closest('.tabs').next('.tabs-wrapper').find('.tab-content').eq($(this).parent().index()).addClass('active');
			$(this).closest('.tabs').next('.tabs-wrapper').find($(this).attr('href')).addClass('active');
		});

		// mobile floorplans tabs hack
		$('.floorplans-container .tabs-wrapper .mobile-tab-trigger').on('click touch', function(event) {
			event.preventDefault();
			$(this).closest('.floorplans-container').find('.tabs').find('a[href="'+ $(this).attr('href') +'"]').click();
			$(this).siblings('.mobile-tab-trigger').removeClass('active');
			$(this).addClass('active');
			// $(this).siblings('.tab-content').slideUp(300);
			// $(this).next('.tab-content').slideDown(300);
			$('html,body').animate({scrollTop:$(this).offset().top - $('#header').height()}, 300);
		});

		// floorplan single height hack
		$(window).on('load resize', function() {

			if ( $(document).height() < $(window).height() ) {
				$('#fp-single .fp-single-details').css('min-height', $(window).height() - ( $('#header').outerHeight() + $('#footer').outerHeight() ));
				$('#fp-single .fp-single-main').css('height', $(window).height() - ( $('#header').outerHeight() + $('#footer').outerHeight() ));

			} else {
				$('#fp-single .fp-single-details').css('min-height', $(document).height() - ( $('#header').outerHeight() + $('#footer').outerHeight() ));
				$('#fp-single .fp-single-main').css('height', $(document).height() - ( $('#header').outerHeight() + $('#footer').outerHeight() ));
			}

			// $('#fp-single .fp-single-main').css('margin-top', ( $('#fp-single').height() - $('#fp-single .fp-single-main').height() ) / 2 );
		});


	});
	
})(jQuery);