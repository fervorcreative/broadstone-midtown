# Razz Interactive Floor Plan Browser

A WordPress plugin for [Razz Interactive](http://razzinteractive.com) clients to connect their site to the Razz Interactive Floor Plan Browser.

## Installation
1. Install and activate [Advanced Custom Fields](https://www.advancedcustomfields.com/) if you don't have it already.
2. Install and activate this plugin.
3. Visit the settings page **(Settings -> Razz Interactive Floor Plan Browser)** and enter the Property ID provided to you by [Razz Interactive](http://razzinteractive.com).
4. Link to `#!/floor-plans` from anywhere on your site you wish, and it will open the floor plan browser when clicked.
