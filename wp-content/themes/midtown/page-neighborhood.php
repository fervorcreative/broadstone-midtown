<?php
/**
 * Template Name: Neighborhood
 *
 */

get_header(); ?>
	
	<section class="page-hero">
		<?php $hero = get_field('page_hero'); ?>
		<img src="<?php echo $hero['url']; ?>" width="100%" height="auto" alt="<?php echo $hero['title']; ?>" />
	</section> 
	
	<section class="page-content amenities-content">
		<div class="row">
			<div class="small-12 medium-8 medium-centered columns page-intro">
				<h2 class="community"><?php the_field('page_headline'); ?></h2>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
				<?php endif; ?>
				<a href="https://goo.gl/maps/pdVP5iHXKCP2" class="purple-button">Directions</a>
			</div>
		</div>
	</section>
	
	<section class="map-content">
		<div id="neighborhood-map"></div>
	</section>
	<section class="community-locations">
		<ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-5 locations-filters">
		<?php
		    // Get WPSEO Locations Categories
		    $exclude = get_term_by('slug', 'home', 'wpseo_locations_category');
		    $category_args = array (
		      'taxonomy' => 'wpseo_locations_category',
		      'exclude' => $exclude->term_id,
		      'hide_empty' => 0,
		      'orderby' => 'slug',
		      );
		    $categories = get_categories($category_args);
			?>
        	<?php foreach ($categories as $category) :
			$locations = get_locations( $category ); ?>
			<li id="<?php echo $category->category_nicename; ?>" class="locations-category map__filter-<?php echo $category->category_nicename; ?>" data-category="<?php echo $category->category_nicename; ?>">
				<div class="content" id="<?php echo $category->slug; ?>">
	                <h3 class="<?php echo $category->slug; ?>"><?php echo $category->name; ?></h3>
	                <ul>
	                  <?php foreach ( $locations as $location ) : ?>
	                    <li>
		                    <a href="#" data-location-id="<?php echo $location->ID; ?>"><?php echo $location->post_title; ?></a>  	
		                </li>
	                  <?php endforeach; ?>
	                </ul>
                </div>  
			</li>
		<?php endforeach; ?>	
		</ul>
	</section>
	
	<section class="amenities-carousel">
		<div class="cycle-slideshow" 
			data-cycle-timeout="0" 
			data-cycle-speed="500"
			data-cycle-fx="carousel"
            data-cycle-carousel-visible="1"
            data-cycle-carousel-fluid="true"
			data-cycle-slides="div.slide-container">
			<span class="cycle-prev"></span>
			<span class="cycle-next"></span>
			<?php $images = get_field('neighborhood_gallery');
				if( $images ): ?>
				<?php foreach( $images as $image ): ?>
				<div class="slide-container">
					<img src="<?php echo $image['url']; ?>" width="100%" height="auto" alt="<?php echo $image['title']; ?>" />
				</div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</section>
	
<?php
get_footer();