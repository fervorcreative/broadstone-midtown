<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Brickell
 */

?>

	</div><!-- #content -->
	<footer class="site-footer">
		<div class="row">
			<div class="small-12 columns footer-logo">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<img src="<?php bloginfo('template_directory'); ?>/images/midtown-logo.svg" width="100%" height="auto" alt="Broadstone Midtown" />
				</a>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns footer-text">
				<?php the_field('address', 'options'); ?> &nbsp;|&nbsp; <?php the_field('phone_number', 'options'); ?> &nbsp;|&nbsp; Professionally Managed by <a href="http://www.liveatalliance.com">Alliance Residential</a> &nbsp;|&nbsp; <a href="http://www.liveatalliance.com/privacy-policy">Privacy Policy</a> &nbsp;|&nbsp; <a href="http://www.liveatalliance.com/terms">Legal</a> &nbsp;|&nbsp; <a href="http://www.liveatalliance.com/fair-housing">Fair Housing</a>
			</div>
		</div>
		<div class="riow">
			<div class="small-12 columns alliance-logo">
				<img src="<?php bloginfo('template_directory'); ?>/images/alliance-eh-logo.svg" width="100%" height="auto" alt="Alliance Residential Logo + Equal Housing Logo" class="a-logo" /><img src="<?php bloginfo('template_directory'); ?>/images/ngbs-logo.png" width="100%" height="auto" alt="NGBS Green Certified" class="n-logo" />
			</div>
		</div>
	</footer>
	
	
	<div id="remarketing" class="remarketing mfp-hide mfp-popup">
		<h2>Privacy Policy – Site Remarketing</h2>
		<h3>This Website Uses Remarketing</h3>
		<p>This website uses remarketing to advertise on third party websites (including Google and Facebook) to previous visitors to our site. It could mean that we advertise to previous visitors who haven’t completed a task on our site, for example using the contact form to make an enquiry. This could be in the form of an advertisement on the Google search results page, a site in the Google Display Network, or a post in the Facebook News Feed. Third­ party vendors, including Google and Facebook, use cookies to serve ads based on someone’s past visits to this website. Of course, any data collected will be used in accordance with our own privacy policy and Google and Facebook’s privacy policy. You can set preferences for how Google advertises to you using the <a href="https://www.google.com/settings/u/0/ads/authenticated" target="_blank">Google Ad Preferences page</a>.</p>
	</div>

<?php wp_footer(); ?>
<script>
jQuery(document).ready(function() {

	jQuery("#contact-form").validate({
		rules: {
			firstName: "required",
			lastName: "required",
			phone: "required",
			email: "required",
			message: "required"
		}
	});
});
						</script>
</body>
</html>
