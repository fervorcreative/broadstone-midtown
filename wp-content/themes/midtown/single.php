<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package falconlanding
 */

get_header(); ?>

	<div class="content-area page-content blog-page-content">
		
		<?php while ( have_posts() ) : the_post(); ?>
		
		<div class="row">
			<div class="small-12 medium-centered columns featured-post">
				<div class="featured-post-hero">
					<a href="<?php echo get_permalink(); ?>"><?php the_post_thumbnail( 'featured-post' ); ?></a>
				</div>
				<div class="featured-post-meta">
					<span><?php the_category( ', ' ); ?> &ndash; <?php the_date(); ?></span><br />
					<a href="<?php echo get_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
				</div>	
			</div>
		</div>
		<div class="row">
			<div class="small-12 medium-9 medium-centered columns post-single">
				<?php the_content(); ?>
			</div>
		</div>
		<div class="row">
			<div class="small-12 small-centered columns">
				<?php the_post_navigation(); ?>
			</div>
		</div>
		<div class="row">
			<div class="small-12 small-centered columns back-to-blog">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>blog">Back to all posts</a>
			</div>
		</div>
		
		<?php endwhile; // End of the loop. ?>
	
	</div>

<?php
get_footer();
