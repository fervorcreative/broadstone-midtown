<html>
	<head>
		<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js?ver=1.11.1'></script>
		<script type='text/javascript' src='<?php bloginfo('template_directory'); ?>/js/jquery.cycle2.min.js'></script>
	</head>
	<body>	
		<div class="floorplan-modal-content">
			<div class="row">
				<div class="modal-left">
					<?php 
						$images = get_field('floorplan_image'); 
						$level = get_field('floor_level');
						$sqft = get_field('square_footage');
						$price = get_field('price_range');
						$bed = get_field('bedrooms');
						$bath = get_field('bathrooms'); 
						$availability = get_field('availability_link');
						?>
					<div class="floor-level">
						<img src="<?php echo $level['url']; ?>" width="100%" height="auto" alt="<?php echo $level['title']; ?>" />
					</div>
					<div class="top-table">
						<div class="top-left-col">
					 		<h2><?php the_title(); ?></h2>
						</div>
						<div class="top-right-col">
							<a href="<?php echo $availability; ?>" class="availability-link" target="_blank">Availability</a>
						</div>
					</div>
					<div class="bottom-table">
						<div class="bottom-left">
							<div class="bottom-left-col">
								<?php echo $sqft; ?> SQ FT
							</div>
							<div class="bottom-left-col-2">
								<?php echo $price; ?>
							</div>
						</div>
						<div class="bottom-right-col">
							<?php echo $bed; ?><br /><?php echo $bath; ?>
						</div>
					</div>
					<div class="site-plan">
						<a href="<?php bloginfo('template_directory'); ?>/pdf/brickell-siteplan.pdf" target="_blank">View Site Plan</a>
					</div>
				</div>
				<div class="modal-right">
				<?php if( $images ): ?>
					<div class="cycle-slideshow" 
						data-cycle-fx="fade"
						data-cycle-timeout="4000" 
						data-cycle-speed="1000"
						data-cycle-slides="div.slide-container" >
						<?php foreach( $images as $image ): ?>
						<div class="slide-container">
							<img src="<?php echo $image['url']; ?>" width="100%" height="auto" alt="<?php echo $image['title']; ?>" />
						</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
				</div>
			</div>
		</div>
	</body>
</html>
