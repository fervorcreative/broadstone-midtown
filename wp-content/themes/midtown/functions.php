<?php
/**
 * midtown functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package midtown
 */

if ( ! function_exists( 'midtown_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function midtown_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on midtown, use a find and replace
	 * to change 'midtown' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'midtown', get_template_directory() . '/languages' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Main Menu', 'midtown' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
	
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'gallery-full', 1030, 585, true );
	add_image_size( 'gallery-thumb', 360, 230, true );
	add_image_size( 'featured-post', 1030, 500, true );
	add_image_size( 'post-thumb', 290, 220, true );
}
endif;
add_action( 'after_setup_theme', 'midtown_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function midtown_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'midtown_content_width', 1280 );
}
add_action( 'after_setup_theme', 'midtown_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function midtown_scripts() {
	
	wp_enqueue_style( 'midtown-style', get_stylesheet_uri() );
	wp_enqueue_style( 'main-css', get_template_directory_uri() . '/css/app.css');
	wp_enqueue_style( 'fonts', '//fast.fonts.net/cssapi/982026e4-bd78-45ae-a7ca-724420ed0fce.css');
	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Raleway:400,600,800');
	
	// Header Scripts
	if ( !is_admin() ) {
	  wp_deregister_script('jquery');
	    wp_register_script('jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '1.11.1', false );
	    wp_register_script('jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.min.js', false, '1.11.1', false );
	    wp_enqueue_script('jquery');
	}
	
	// Footer Scripts
	//wp_enqueue_script( 'sharethis-buttons', 'http://w.sharethis.com/button/buttons.js', false, false, true );
	//wp_enqueue_script( 'sharethis-loader', 'http://s.sharethis.com/loader.js', false, false, true );
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.js', array('jquery'), '2.8.2', true );
	wp_enqueue_script( 'foundation', get_template_directory_uri() . '/js/foundation.min.js', array('jquery'), '5.3', true );
	wp_enqueue_script( 'tabs', get_template_directory_uri() . '/js/foundation.tab.js', array('jquery'), '5.3', true );
	wp_enqueue_script( 'cycle', get_template_directory_uri() . '/js/jquery.cycle2.min.js', array('jquery'), '2.1.5', true );
	wp_enqueue_script( 'cycle-carousel', get_template_directory_uri() . '/js/jquery.cycle2.carousel.js', array('jquery'), '2.0.0', true );
	wp_enqueue_script( 'magnific', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array('jquery'), '1.0.0', true );
	wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick.min.js', array('jquery'), '1.6.0', true );
	wp_enqueue_script( 'instafeed', get_template_directory_uri() . '/js/instafeed.min.js', array('jquery'), '1.4.1', true );
	wp_enqueue_script( 'scrollmagic', get_template_directory_uri() . '/js/scrollmagic.min.js', array('jquery'), '2.0.5', true );
	//wp_enqueue_script( 'gmaps', 'http://maps.googleapis.com/maps/api/js?v=3.9?key=AIzaSyC13nd2Q0g_KPqAu_2X1PEz-jNV-sYalGM', false, false, true );
	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array('jquery'), '1.0', true );
	
	if ( is_page( 'the-city') ) {
		// Send Locations to JS
		  $data = array(
		    'template_dir' => get_bloginfo( 'template_directory' )
		  );
		
	    $data['locations'] = get_locations();
	    wp_localize_script( 'main', 'AppData', $data );
	}
    
}
add_action( 'wp_enqueue_scripts', 'midtown_scripts' );

// Add custom post type for floorplans
function create_post_types() {
	
	register_post_type('Floorplans', array(
		'labels' => array(
			'name' => 'Floorplans',
			'singular_name' => 'Floorplan',
			'menu_name' => 'Floorplans',
			'add_new' => 'Add Floorplan',
			'add_new_item' => 'Add New Floorplan',
			'edit' => 'Edit',
			'edit_item' => 'Edit Floorplans',
			'new_item' => 'New Floorplan',
			'view' => 'View Floorplan',
			'view_item' => 'View Floorplan',
			'search_items' => 'Search Floorplans',
			'not_found' => 'No Floorplans Found',
			'not_found_in_trash' => 'No Floorplans Found in Trash',
			'parent' => 'Parent Floorplan',
		),
		'public' => true,
		'query_var' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'rewrite' => array('slug' => '/floorplans'),
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
		),
		'menu_position' => 7, 
	));

}
add_action( 'init', 'create_post_types' );

// Map Locations 

function get_locations( $category = null ) {
  $query_args = array(
    'post_type' => 'wpseo_locations',
    'orderby' => 'title',
    'order' => 'ASC',
    'posts_per_page' => -1,
    'fields' => 'wpseo_coordinates_lat',
  );

  if ( $category ) {
    $query_args['tax_query'] = array (
      array (
          'taxonomy' => 'wpseo_locations_category',
          'field' => 'slug',
          'terms' => $category->slug,
        )
    );
  }

  $query = new WP_Query( $query_args );

  // Append custom fields and categories
  foreach ( $query->posts as $i => $post ) {
    // Get custom fields values
    $custom_fields = get_post_custom( $post->ID );
    foreach ( $custom_fields as $j => $field ) {
      if ( 1 == count( $field ) ) {
        $custom_fields[$j] = reset( $field );
      }
    }

    // Append custom fields
    $query->posts[$i]->custom_fields = $custom_fields;

    // Get category and append
    $category = get_the_terms( $post->ID, 'wpseo_locations_category' );
    if ( 1 == count( $category ) ) {
      $category = reset( $category );
    }
    $query->posts[$i]->category = $category;
  }

  return $query->posts;
}

// Add SVG support to Wordpress uploader

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function custom_admin_head() {
  $css = '';

  $css = 'td.media-icon img[src$=".svg"] { width: 100% !important; height: auto !important; }';

  echo '<style type="text/css">'.$css.'</style>';
}
add_action('admin_head', 'custom_admin_head');

// WP SEO
add_filter( 'wpseo_metabox_prio', function() { return 'low'; });
add_filter( 'wpseo_use_page_analysis', function() { return false; });

// ACF Options Page
if( function_exists('acf_add_options_page') ) {	
	acf_add_options_page('Theme Options');
}

// Remove Emoji garbage
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

// SHORTCODE: Rent Cafe Form
function contact_form_func() {
    ob_start();
    {
		
// Leads API User Info (lead Request Type) - (Contact us form - Requests guest card information from third-party providers.)
// FName: api
// LName: Leads
// Username: apileads@allresco.com
// Password: leads5678
// Email: apileads@allresco.com
// Security question: What was your first pet’s name?
// Security Answer: dog



// $url = 'https://api.rentcafe.com/rentcafeapi.aspx?requestType=lead&firstName='.filter_var($_POST['firstName'], FILTER_SANITIZE_STRING).'&lastName='.filter_var($_POST['lastName'], FILTER_SANITIZE_STRING);
// $url .= '&email='.$_POST['email'].'&phone='.$_POST['phone'].'&message='.$_POST['message'];
// $url .= '&propertyCode=p0536183&username=dwightleads@allresco.com&password=Alliance&source=dwight-website';
// // $url .= '&propertyCode=p0000156&username=leadsapi@yardi.com&password=password&source=dwight-website';
// $url .= '&addr1='.$_POST['addr1'].'&addr2='.$_POST['addr2'].'&city='.$_POST['city'].'&='.$_POST['state'].'&ZIPCode='.$_POST['ZIPCode'];

$url = 'https://api.rentcafe.com/rentcafeapi.aspx?requestType=lead&firstName='.filter_var($_POST['firstName'], FILTER_SANITIZE_STRING).'&lastName='.filter_var($_POST['lastName'], FILTER_SANITIZE_STRING);
$url .= '&email='.$_POST['email'].'&phone='.$_POST['phone'].'&message='.$_POST['message'];
$url .= '&propertyCode=p0859483&APIToken=4bab4a92-2c87-4743-87e6-853a377f90bc&username=apileadsalliance@allresco.com&password=leads1234&source=Internet-Property Website-LAA';
// $url .= '&propertyCode=p0000156&username=leadsapi@yardi.com&password=password&source=dwight-website';
$url .= '&addr1="&addr2="&city="&state="&ZIPCode="';

?>

	<?php	
				
							if (isset($_POST) && !empty($_POST)) {
								// http://docs.guzzlephp.org/en/latest/
										require_once($_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/midtown/vendor/autoload.php'); 
								
								

								$client = new GuzzleHttp\Client();
								$res = $client->request('GET', $url);

								if ($res->getBody() == 'Success') {
									echo 'Your request was received successfully! We also invite you to <a href="https://villastechridge.securecafe.com/onlineleasing/villas-at-tech-ridge/register.aspx" target="_blank">apply</a> for your<br/>new home by completing an online application.';
									header("Location: https://broadstonemidtown.com/thank-you");

								} else {
									echo 'There was an error while trying to submit your request.' .$res->getBody();
								}
							} else {
								echo $intro[0]['content'];
							}
						
				

if (empty($_POST)) :
				



			?>
						<form method="post" enctype="multipart/form-data" id="contact-form" action="">
		                        <div class="gform_body">
									<ul id="gform_fields_1" class="gform_fields top_label form_sublabel_below description_below">
										<li id="field_1_1" class="gfield one-half fname gfield_contains_required field_sublabel_below field_description_below">
											<label class="gfield_label" for="firstName">First Name*<span class="gfield_required">*</span></label>
											<div class="ginput_container ginput_container_text"><input name="firstName" id="firstName" type="text" class="medium" placeholder="First Name*" aria-required="true" aria-invalid="false" maxlength="40" tabindex="0"></div>
										</li>
										
										<li id="field_1_2" class="gfield one-half lname gfield_contains_required field_sublabel_below field_description_below">
											<label class="gfield_label" for="lastName">Last Name*<span class="gfield_required">*</span></label>
											<div class="ginput_container ginput_container_text"><input name="lastName" id="lastName" type="text" class="medium" placeholder="Last Name*" aria-required="true" aria-invalid="false"  maxlength="40" tabindex="1"></div>
										</li>
										
										<li id="field_1_3" class="gfield one-half phone gfield_contains_required field_sublabel_below field_description_below">
											<label class="gfield_label" for="phone">Phone Number*<span class="gfield_required">*</span></label>
											<div class="ginput_container ginput_container_text"><input name="phone" id="phone" type="text" class="medium" placeholder="Phone Number*" aria-required="true" aria-invalid="false" maxlength="17" tabindex="2"></div>
										</li>
										<li id="field_1_4" class="gfield one-half email gfield_contains_required field_sublabel_below field_description_below">
											<label class="gfield_label" for="email">Email Address*<span class="gfield_required" maxlength="80">*</span></label>
											<div class="ginput_container ginput_container_email">
		                            		<input name="email" id="email" type="email" class="medium email" placeholder="Email Address*" tabindex="3">
		                        			</div>
										</li>

									
										<li id="field_1_7" class="gfield full-width clear field_sublabel_below field_description_below">
											<label class="gfield_label" for="message">Message*</label><div class="ginput_container">
											<textarea name="message" id="message" class="textarea medium" placeholder="Message*" aria-invalid="false" rows="5" cols="50" tabindex="4"></textarea></div>
										</li>
							
										<li id="field_1_8" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below">
											<em>By clicking submit, you agree that you are the individual whose information has been entered above or that you have that individual's express consent to submit their information. Additionally, you agree and acknowledge that by submitting this information, you may be contacted regarding your inquiry for more information from the relevant property.</em>
										</li>
		                            </ul></div>
						        <div class="gform_footer top_label">
									<input type="submit" id="gform_submit_button_2" class="gform_button button submit" value="Submit" />


						        </div>
                        </form>


<?php

endif;
				
						
		
    return ob_get_clean();
    }
}
add_shortcode( 'contact_form', 'contact_form_func' );

?>
