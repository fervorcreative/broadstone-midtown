<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package falconlanding
 */

get_header(); ?>

	<div class="content-area page-content blog-page-content">
		<div class="row">
			<div class="small-12 medium-centered columns featured-post">
				
			<?php 
				//Define your custom post type name in the arguments
				 
				$args = array('post_type' => 'post','posts_per_page'=>'1','order'=>'DESC','orderby' => 'date');
				 
				//Define the loop based on arguments
				 
				$loop = new WP_Query( $args );
				 
				//Display the contents
				 
				while ( $loop->have_posts() ) : $loop->the_post();
				?>
				
				<div class="featured-post-hero">
					<a href="<?php echo get_permalink(); ?>"><?php the_post_thumbnail( 'featured-post' ); ?></a>
				</div>
				<div class="featured-post-meta">
					<span><?php the_category( ', ' ); ?> &ndash; <?php the_date(); ?></span><br />
					<a href="<?php echo get_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
				</div>
				<?php $exclude = get_the_ID(); ?>
			<?php endwhile;?>
			<?php wp_reset_postdata(); ?>
				<hr />
			</div>
		</div>
		<div class="row">
			<div class="small-12 medium-3 columns posts-sidebar">
				<ul>
					<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>blog/">All</a></li>
				    <?php wp_list_categories( array(
				        'orderby'    => 'name',
				        'title_li' => ''
				    ) ); ?> 
				</ul>
			</div>
			<div class="small-12 medium-9 columns posts-list">
				<ul>
					<?php 
					//Define your custom post type name in the arguments
					$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
					$args = array('post_type' => 'post','posts_per_page'=>'3', 'paged' => $paged, 'order'=>'DESC','orderby' => 'date', 'post__not_in' => array($exclude) );
					 
					//Define the loop based on arguments
					 
					$wp_query = new WP_Query( $args );
					 
					//Display the contents
					  if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
					?>
				
					<li class="post-item">
						<div class="small-12 medium-4 columns content-left">
							<a href="<?php echo get_permalink(); ?>"><?php the_post_thumbnail( 'post-thumb' ); ?></a>
						</div>
						<div class="small-12 medium-8 columns content-right">
							<span><?php the_category( ', ' ); ?> &ndash; <?php the_date(); ?></span>
							<a href="<?php echo get_permalink(); ?>" class="post-title"><h4><?php the_title(); ?></h4></a>
							<?php the_excerpt(); ?>
							<?php /* <a href="<?php echo get_permalink(); ?>" class="read-more">Read more</a> */ ?>
						</div>
					</li>
				<?php endwhile;?>
				<?php the_posts_pagination( array( 'mid_size'  => 2 ) ); ?>
				<?php endif; wp_reset_postdata(); ?>	
				</ul>
			</div>
		</div>
	</div>

<?php
get_sidebar();
get_footer();
