<?php get_header(); ?>
	
	<section class="home-grid">
		<ul class="small-block-grid-1 medium-block-grid-2">
			<li class="amenities">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>amenities">
					<div class="content">
						<h3><?php the_field('box_1_title'); ?></h3>
						<p><?php the_field('box_1_text'); ?></p>
					</div>
				</a>
			</li>
			<li class="residences">
				<a href="#!/floor-plans">
					<div class="content">
						<h3><?php the_field('box_2_title'); ?></h3>
						<p><?php the_field('box_2_text'); ?></p>
					</div>
				</a>
			</li>
			<li class="community">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>the-city">
					<div class="content">
						<h3><?php the_field('box_3_title'); ?></h3>
						<p><?php the_field('box_3_text'); ?></p>
					</div>
				</a>
			</li>
			<li class="contact">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>contact">
					<div class="content">
						<h3><?php the_field('box_4_title'); ?></h3>
						<p><?php the_field('box_4_text'); ?></p>
					</div>
				</a>
			</li>
		</ul>
	</section>
	
	<section class="home-map-content">
		<div id="home-map"></div>
	</section>
	
	<section class="instagram-grid">
		<ul class="small-block-grid-2 medium-block-grid-3" id="instafeed">
		</ul>
	</section>
	
<?php
get_footer();
