<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Brickell
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5C2JV29');</script>
<!-- End Google Tag Manager -->
<meta name="google-site-verification" content="Em2JLAI1r36tbH3ARU_xceKEunlf489h7Ypjs-wQ1JQ" />
<?php wp_head(); ?>
<?php /* <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86024012-1', 'auto');
  ga('send', 'pageview');
</script> */ ?>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/plugins/jquery.validate.js"></script>
</head>

<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5C2JV29"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<!-- Site Promotional Banner -->
	<?php if ( is_front_page() ) { ?>
		<?php if( get_field('headline', 'options') ): ?>
		<div class="top-banner">
			<h1><?php the_field('headline', 'options'); ?></h1>
	        <p class="restrictions"><?php the_field('disclaimer', 'options'); ?></p>
		</div>
		<?php endif; ?>
	<?php } ?>

	<div class="site-menu">
		<div class="row">
			<div class="small-12 large-4 large-centered columns menu-container">
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => 'ul', 'container_class' => 'nav-bar' ) ); ?>
				<?php if( get_field('application_link', 'options') ): ?>
				<a href="<?php the_field('application_link', 'options'); ?>" class="apply-now-mobile">Apply Now</a>
				<?php endif; ?>
				<a href="https://popcard.rentcafe.com/TextUsWidget.aspx?dnis=8552579805" class="text-us-mobile" title="Link opens in a new window" target="_blank">Text Us</a>
				<a href="tel:<?php the_field('phone_number', 'options'); ?>" class="phone-mobile"><?php the_field('phone_number', 'options'); ?></a>
				<div class="social-container">
					<?php
						$facebook = get_field('facebook', 'options');
						$twitter = get_field('twitter', 'options');
						$googleplus = get_field('google_plus', 'options');
						$instagram = get_field('instagram', 'options');
						?>
					<h4>Stay Connected</h4>
					<ul class="nav-social">
						<li class="twitter">
							<a href="<?php echo $twitter; ?>"><span>Twitter</span></a>
						</li>
						<li class="facebook">
							<a href="<?php echo $facebook; ?>"><span>Facebook</span></a>
						</li>
						<li class="instagram">
							<a href="<?php echo $instagram; ?>"><span>Instagram</span></a>
						</li>
						<li class="google-plus">
							<a href="<?php echo $googleplus; ?>"><span>Google Plus</span></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<header class="site-header">
		<div class="header-content">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="header-logo">
				<img src="<?php bloginfo('template_directory'); ?>/images/midtown-logo.svg" alt="Broadstone Midtown" />
			</a>
			<ul class="secondary-nav">
				<li><a href="https://popcard.rentcafe.com/TextUsWidget.aspx?dnis=8552579805" class="text-us" title="Link opens in a new window" target="_blank">Text Us</a></li>
				<li><a href="<?php the_field('application_link', 'options'); ?>" class="apply-now">Apply Now</a></li>
				<li><a href="tel:<?php the_field('phone_number', 'options'); ?>" class="phone"><?php the_field('phone_number', 'options'); ?></a></li>
			</ul>
			<div class="nav-toggle-container">
				<div class="nav-toggle">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>
		</div>
	</header>
	<?php if ( is_front_page() ) { ?>
	<div class="home-hero">
		<img src="<?php bloginfo('template_directory'); ?>/images/hero-midtown.jpg" width="100%" height="auto" alt="Hero Home" />
		<a href="https://broadstonemidtown.com/#!/floor-plans" class="apply-link">
			<img src="<?php bloginfo('template_directory'); ?>/images/midtown-logomark.svg" alt="Broadstone Midtown" />
			<span>CHECK AVAILABILITY</span>
			<?php /* <span>Make A Reservation</span> */ ?>
		</a>
	</div>
	<?php } ?>
	<div class="site-content">
