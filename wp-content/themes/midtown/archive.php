<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package falconlanding
 */

get_header(); ?>

	<div class="content-area page-content blog-page-content">
		<div class="row archive-header">
			<div class="small-12 medium-9 medium-centered columns page-intro">
				<?php if ( have_posts() ) : ?>
				<?php
					the_archive_title( '<h3 class="page-title">', '</h3>' );
				?>
			</div>
		</div>
		<div class="row">
			<div class="small-12 medium-3 columns posts-sidebar">
				<ul>
					<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>blog/">All</a></li>
				    <?php wp_list_categories( array(
				        'orderby'    => 'name',
				        'title_li' => ''
				    ) ); ?> 
				</ul>
			</div>
			<div class="small-12 medium-9 columns posts-list">
				<ul>
				<?php while ( have_posts() ) : the_post(); ?>
					
					<li class="post-item">
						<div class="small-12 medium-4 columns content-left">
							<a href="<?php echo get_permalink(); ?>"><?php the_post_thumbnail( 'post-thumb' ); ?></a>
						</div>
						<div class="small-12 medium-8 columns content-right">
							<span><?php the_category( ', ' ); ?> &ndash; <?php the_date(); ?></span>
							<a href="<?php echo get_permalink(); ?>" class="post-title"><h4><?php the_title(); ?></h4></a>
							<?php the_excerpt(); ?>
							<?php /* <a href="<?php echo get_permalink(); ?>" class="read-more">Read more</a> */ ?>
						</div>
					</li>

				<?php endwhile; ?>
				</ul>
				
				<?php the_posts_pagination( array(
	'mid_size'  => 2,
	'prev_text' => __( 'Back', 'textdomain' ),
	'next_text' => __( 'Onward', 'textdomain' ),
) ); ?>
				<?php else :
		
					get_template_part( 'template-parts/content', 'none' );
		
				endif; ?>
				
			</div>
		</div>
	</div>

<?php
get_sidebar();
get_footer();
