<?php
/**
 * Template Name: Gallery
 *
 */

get_header(); ?>
	
	<section class="gallery-content">
		<div class="row gallery-row">
			<div class="tab-container">
				<div class="tabs-content">
					<div class="tab-content content active" id="community">
						<div class="gallery-carousel carousel-1">
						<?php $images = get_field('community_gallery');
							if( $images ): ?>
							<?php foreach( $images as $image ): ?>
							<div class="carousel-units">
								<img src="<?php echo $image['sizes']['gallery-full']; ?>" alt="<?php echo $image['title']; ?>" />
							</div>
							<?php endforeach; ?>
						<?php endif; ?>
						</div>
					</div>
					<div class="tab-content content" id="interior" style="position:relative;">
						<img src="<?php bloginfo('template_directory'); ?>/images/gallery-placeholder.png" width="100%" height="auto" style="max-width:1030px; margin: 0 auto;" />
						<div class="gallery-carousel carousel-2" style="position: absolute; top:0;left: 0; right:0;">
						<?php $images = get_field('interior_gallery');
							if( $images ): ?>
							<?php foreach( $images as $image ): ?>
							<div class="carousel-units">
								<img src="<?php echo $image['sizes']['gallery-full']; ?>" alt="<?php echo $image['title']; ?>" />
							</div>
							<?php endforeach; ?>
						<?php endif; ?>
						</div>
					</div>
					<div class="tab-content content" id="exterior">
						<div class="gallery-carousel carousel-3">
						<?php $images = get_field('exterior_gallery');
							if( $images ): ?>
							<?php foreach( $images as $image ): ?>
							<div class="carousel-units">
								<img src="<?php echo $image['sizes']['gallery-full']; ?>" alt="<?php echo $image['title']; ?>" />
							</div>
							<?php endforeach; ?>
						<?php endif; ?>
						</div>
					</div>
					<div class="tab-content content" id="virtual-tour">
						<div class="gallery-carousel carousel-4">
						<?php if( have_rows('virtual_tour_gallery') ): ?>
							<?php while( have_rows('virtual_tour_gallery') ): the_row(); ?>
							<?php
								$tour_image = get_sub_field('virtual_tour_image');
								$tour_embed = get_sub_field('virtual_tour_embed_url');
								?>
							<div class="carousel-units tour">
								<a href="#" class="virtual-tour-link" data-video-id="<?php echo $tour_embed; ?>"><span>View Virtual Tour</span></a>
								<img src="<?php echo $tour_image['sizes']['gallery-full']; ?>" alt="<?php echo $tour_image['title']; ?>" />
							</div>
							<?php endwhile; ?>
						<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="tabs-content">
				<ul class="tabs" data-tab>
					<li class="tab tab-title"><a href="#videos">Videos</a></li><li class="tab tab-title active" ><a href="#community">Community</a></li><li class="tab tab-title"><a href="#interior" id="interior-button">Interior</a></li><li class="tab tab-title"><a href="#exterior">Exterior</a></li><li class="tab tab-title"><a href="#virtual-tour">Virtual Tours</a></li>
				</ul>
				<div class="tabs-content">
					<div class="tab-content content" id="videos">
					<?php if( have_rows('video_gallery') ): ?>
						<ul class="small-block-grid-2 medium-block-grid-4 thumb-pager">
						<?php while( have_rows('video_gallery') ): the_row(); ?>
							<?php
								$thumb = get_sub_field('video_thumbnail');
								$link = get_sub_field('video_link');
								?>
							<li>
								<a href="<?php echo $link; ?>" class="video-link">
									<img src="<?php echo $thumb['sizes']['gallery-thumb']; ?>" width="100%" height="auto" alt="<?php echo $thumb['title']; ?>" />
								</a>
							</li>
						<?php endwhile; ?>
						</ul>
					<?php endif; ?>
					</div>
					<div class="tab-content content active" id="community">
						<?php $images = get_field('community_gallery');
							if( $images ): ?>
						<ul class="small-block-grid-2 medium-block-grid-4 thumb-pager thumb-pager-1">
							<?php $i=0; ?>
							<?php foreach( $images as $image ): ?>
							<li>
								<a href="#" class="slick-link" onclick="$('.carousel-1').slick('slickGoTo','<?php echo $i++; ?>');">
									<img src="<?php echo $image['sizes']['gallery-thumb']; ?>" width="100%" height="auto" alt="<?php echo $image['title']; ?>" />
								</a>
							</li>
							<?php endforeach; ?>
						</ul>
						<?php endif; ?>
					</div>
					<div class="tab-content content" id="interior">
						<?php $images = get_field('interior_gallery');
							if( $images ): ?>
						<ul class="small-block-grid-2 medium-block-grid-4 thumb-pager thumb-pager-2">
							<?php $i=0; ?>
							<?php foreach( $images as $image ): ?>
							<li>
								<a href="#" class="slick-link" onclick="$('.carousel-2').slick('slickGoTo','<?php echo $i++; ?>');">
									<img src="<?php echo $image['sizes']['gallery-thumb']; ?>" width="100%" height="auto" alt="<?php echo $image['title']; ?>" />
								</a>
							</li>
							<?php endforeach; ?>
						</ul>
						<?php endif; ?>
					</div>
					<div class="tab-content content" id="exterior">
						<?php $images = get_field('exterior_gallery');
							if( $images ): ?>
						<ul class="small-block-grid-2 medium-block-grid-4 thumb-pager thumb-pager-3">
							<?php $i=0; ?>
							<?php foreach( $images as $image ): ?>
							<li>
								<a href="#" class="slick-link" onclick="$('.carousel-3').slick('slickGoTo','<?php echo $i++; ?>');">
									<img src="<?php echo $image['sizes']['gallery-thumb']; ?>" width="100%" height="auto" alt="<?php echo $image['title']; ?>" />
								</a>
							</li>
							<?php endforeach; ?>
						</ul>
						<?php endif; ?>
					</div>
					<div class="tab-content content" id="virtual-tour">
					<?php if( have_rows('virtual_tour_gallery') ): ?>
						<ul class="small-block-grid-2 medium-block-grid-4 thumb-pager thumb-pager-4">
						<?php $i=0; ?>
						<?php while( have_rows('virtual_tour_gallery') ): the_row(); ?>
							<?php
								$tour_image = get_sub_field('virtual_tour_image');
								$tour_embed = get_sub_field('virtual_tour_embed_url');
								?>
							<li>
								<a href="#" class="slick-link" onclick="$('.carousel-4').slick('slickGoTo','<?php echo $i++; ?>');">
									<img src="<?php echo $tour_image['sizes']['gallery-thumb']; ?>" width="100%" height="auto" alt="<?php echo $tour_image['title']; ?>" />
								</a>
							</li>
						<?php endwhile; ?>
						</ul>
					<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script>
		$(window).load(function() { 
			$(document).foundation();
		});
	</script>
<?php
get_footer();