<?php
/**
 * Template Name: Residences
 *
 */

get_header(); ?>
	
	<section class="page-hero">
		<?php $hero = get_field('page_hero'); ?>
		<img src="<?php echo $hero['url']; ?>" width="100%" height="auto" alt="<?php echo $hero['title']; ?>" />
	</section> 
	
	<section class="page-content amenities-content">
		<div class="row">
			<div class="small-12 medium-8 medium-centered columns page-intro residences-intro">
				<h2 class="residences"><?php the_field('page_headline'); ?></h2>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
				<?php endif; ?>
				<a href="<?php the_field('application_link', 'options'); ?>" class="blue-button">Reserve Now</a>
			</div>
		</div>
	</section>
	
	<section class="row residences-embed">
		<div class="small-12 columns">
			<iframe src="http://apt.razzinteractive.com/broadstonemidtown/#!/floor-plans/floors" width="100%" height="800"></iframe>
		</div>
	</section>
	
	<?php /* <section class="row residences-content">
		<div class="small-12 columns tab-container residences-tabs">
			<ul class="tabs" data-tab>
				<li class="tab tab-title active"><a href="#one-bedroom">1 Bedroom</a></li><li class="tab tab-title"><a href="#two-bedroom">2 Bedroom</a></li><li class="tab tab-title"><a href="#townhomes">Townhomes</a></li>
			</ul>
			<div class="tabs-content">
				<!-- Studio Tab -->
				<div class="tab-content content active" id="studio">												
					<div class="cycle-slideshow slider-one" 
						data-cycle-fx="fade" 
						data-cycle-slides="div.unit" 
						data-cycle-timeout="0"
						data-cycle-speed="500"
						data-cycle-pager=".slider-one .pager"
						data-cycle-prev=".slider-one .prev"
						data-cycle-next=".slider-one .next" 
					>
						<!--<div class="floorplan-nav">
							<span class="prev"></span>
							<span class="next"></span>
						</div>-->
					
						<?php 
							$args = array( 
								'post_type' => 'floorplans', 
								'meta_query' => array(
									'relation' => 'AND',
									array(
										'key' => 'number_of_bedrooms',
										'value' => 'Studio',
										'compare' => '='
									),
								),
								'posts_per_page' => 20, 
								'order' => 'ASC' 
							);
							$loop = new WP_Query( $args );
							$count=1; 
							while ( $loop->have_posts() ) : $loop->the_post(); 
							
							$image = get_field('floor_plan_image');
							$bedrooms = get_field('number_of_bedrooms');
							$baths = get_field('number_of_bathrooms');
							$sq_foot = get_field('square_footage');
						?>
							
							<div class="studio unit unit-<?php echo $count; ?>"> 
								<div class="small-12 medium-4 medium-offset-1 columns unit-info">
									<h3><?php the_title(); ?></h3>			
									<span><?php echo $bedrooms; ?><br /><?php echo $baths; ?> Bath <br /><?php echo $sq_foot; ?> SQ FT</span><br />
									<a class="blue-button check" href="#">Availability</a>
									<div class="pager"></div>
								</div>
								<div class="small-12 medium-7 columns unit-image">
									<?php if( $image ): ?>
										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									<?php endif; ?>
								</div>
							</div><!-- .floor -->
						<?php 
							$count++; 
							endwhile; 
							wp_reset_query(); 
						?>	
					</div>
				</div>
				<!-- 1 Bedroom Tab -->
				<div class="tab-content content active" id="one-bedroom">													
					<div class="cycle-slideshow slider-two" 
						data-cycle-fx="fade" 
						data-cycle-slides="div.unit" 
						data-cycle-timeout="0"
						data-cycle-speed="500"
						data-cycle-pager=".slider-two .pager"
						data-cycle-prev=".slider-two .prev"
						data-cycle-next=".slider-two .next" 
					>
						<div class="floorplan-nav">
							<span class="prev"></span>
							<span class="next"></span>
						</div>
					
						<?php 
							$args = array( 
								'post_type' => 'floorplans', 
								'meta_query' => array(
									'relation' => 'AND',
									array(
										'key' => 'number_of_bedrooms',
										'value' => '1 Bedroom',
										'compare' => '='
									),
								),
								'posts_per_page' => 20, 
								'order' => 'ASC' 
							);
							$loop = new WP_Query( $args );
							$count=1; 
							while ( $loop->have_posts() ) : $loop->the_post(); 
							
							$image = get_field('floor_plan_image');
							$bedrooms = get_field('number_of_bedrooms');
							$baths = get_field('number_of_bathrooms');
							$sq_foot = get_field('square_footage');
							$title = get_the_title();
							$cleantitle = sanitize_title( $title);
						?>
							
							<div class="one-bedroom unit unit-<?php echo $count; ?>"> 
								<div class="small-12 medium-4 medium-offset-1 columns unit-info">
									<h3><?php the_title(); ?></h3>			
									<span><?php echo $bedrooms; ?><br /><?php echo $baths; ?> Bath <br /><?php echo $sq_foot; ?> SQ FT</span><br />
									<a class="blue-button floorplan-popup" href="<?php bloginfo('template_directory'); ?>/snippets/<?php echo $cleantitle; ?>.html">Availability</a>

									<div class="pager"></div>
								</div>
								<div class="small-12 medium-7 columns unit-image">
									<?php if( $image ): ?>
										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									<?php endif; ?>
								</div>
							</div><!-- .floor -->
						<?php 
							$count++; 
							endwhile; 
							wp_reset_query(); 
						?>	
					</div>
				</div>
				<!-- 2 Bedroom Tab -->
				<div class="tab-content content" id="two-bedroom">													
					<div class="cycle-slideshow slider-three" 
						data-cycle-fx="fade" 
						data-cycle-slides="div.unit" 
						data-cycle-timeout="0"
						data-cycle-speed="500"
						data-cycle-pager=".slider-three .pager"
						data-cycle-prev=".slider-three .prev"
						data-cycle-next=".slider-three .next" 
					>
						<div class="floorplan-nav">
							<span class="prev"></span>
							<span class="next"></span>
						</div>
					
						<?php 
							$args = array( 
								'post_type' => 'floorplans', 
								'meta_query' => array(
									'relation' => 'AND',
									array(
										'key' => 'number_of_bedrooms',
										'value' => '2 Bedroom',
										'compare' => '='
									),
								),
								'posts_per_page' => 20, 
								'order' => 'ASC' 
							);
							$loop = new WP_Query( $args );
							$count=1; 
							while ( $loop->have_posts() ) : $loop->the_post(); 
							
							$image = get_field('floor_plan_image');
							$bedrooms = get_field('number_of_bedrooms');
							$baths = get_field('number_of_bathrooms');
							$sq_foot = get_field('square_footage');
							$title = get_the_title();
							$cleantitle = sanitize_title( $title);
						?>
							
							<div class="two-bedroom unit unit-<?php echo $count; ?>"> 
								<div class="small-12 medium-4 medium-offset-1 columns unit-info">
									<h3><?php the_title(); ?></h3>			
									<span><?php echo $bedrooms; ?>s<br /><?php echo $baths; ?> Bath <br /><?php echo $sq_foot; ?> SQ FT</span><br />
									<a class="blue-button floorplan-popup" href="<?php bloginfo('template_directory'); ?>/snippets/<?php echo $cleantitle; ?>.html">Availability</a>

									<div class="pager"></div>
								</div>
								<div class="small-12 medium-7 columns unit-image">
									<?php if( $image ): ?>
										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									<?php endif; ?>
								</div>
							</div><!-- .floor -->
						<?php 
							$count++; 
							endwhile; 
							wp_reset_query(); 
						?>	
					</div>
				</div>
				<!-- Townhomes -->
				<div class="tab-content content" id="townhomes">													
					<div class="cycle-slideshow slider-four" 
						data-cycle-fx="fade" 
						data-cycle-slides="div.unit" 
						data-cycle-timeout="0"
						data-cycle-speed="500"
						data-cycle-pager=".slider-four .pager"
						data-cycle-prev=".slider-four .prev"
						data-cycle-next=".slider-four .next" 
					>
						<div class="floorplan-nav">
							<span class="prev"></span>
							<span class="next"></span>
						</div>
					
						<?php 
							$args = array( 
								'post_type' => 'floorplans', 
								'meta_query' => array(
									'relation' => 'AND',
									array(
										'key' => 'number_of_bedrooms',
										'value' => 'Townhome',
										'compare' => '='
									),
								),
								'posts_per_page' => 20, 
								'order' => 'ASC' 
							);
							$loop = new WP_Query( $args );
							$count=1; 
							while ( $loop->have_posts() ) : $loop->the_post(); 
							
							$image = get_field('floor_plan_image');
							$bedrooms = get_field('number_of_bedrooms');
							$th_bedrooms = get_field('townhouse_bedrooms');
							$baths = get_field('number_of_bathrooms');
							$sq_foot = get_field('square_footage');
							$title = get_the_title();
							$cleantitle = sanitize_title( $title);
						?>
							
							<div class="townhomes unit unit-<?php echo $count; ?>"> 
								<div class="small-12 medium-4 medium-offset-1 columns unit-info">
									<h3><?php the_title(); ?></h3>			
									<span><?php echo $th_bedrooms; ?><br /><?php echo $baths; ?> Bath <br /><?php echo $sq_foot; ?> SQ FT</span><br />
									<a class="blue-button floorplan-popup" href="<?php bloginfo('template_directory'); ?>/snippets/<?php echo $cleantitle; ?>.html">Availability</a>

									<div class="pager"></div>
								</div>
								<div class="small-12 medium-7 columns unit-image">
									<?php if( $image ): ?>
										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									<?php endif; ?>
								</div>
							</div><!-- .floor -->
						<?php 
							$count++; 
							endwhile; 
							wp_reset_query(); 
						?>	
					</div>
				</div>
			</div>
		</div>
	</section> */ ?>
	
	<section class="residence-amenities">
		<div class="row">
			<div class="small-12 medium-10 medium-centered columns">
				<h2 class="amenities">RESIDENT Amenities</h2>
				<?php if( have_rows('residence_amenities') ): ?>
				<ul>
					<?php while( have_rows('residence_amenities') ): the_row(); ?>
					<li><?php echo the_sub_field('amenity'); ?></li>
					<?php endwhile; ?>
				</ul>
				<?php endif; ?>
			</div>
		</div>
	</section>
	
	<script>
		$(document).ready(function() { 
			$(document).foundation();
		});
	</script>
<?php
get_footer();