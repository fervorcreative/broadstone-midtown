var App = App || {},
    $ = $ || jQuery;

// Opens external links in a new window by default
App.externalLink = function() {

	$("a[href*='http://']:not([href*='"+location.hostname+"'])").attr({target: "_blank", title: "Link opens in new window"});
	$("a[href*='https://']:not([href*='"+location.hostname+"'])").attr({target: "_blank", title: "Link opens in new window"});

}

App.textWindow = function() {

    $('.text-us, .text-us-mobile').on('click', function(e) {
        e.preventDefault();
        var href = $(this).attr('href');
        var params = 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=530,height=570';
  window.open(href, 'Send a Text', params);
    });


}

// Mobile menu toggle
App.menuToggle = function() {

	$(".nav-toggle").click(function () {
		$(".site-menu").toggleClass('visible');
		$(".nav-toggle").toggleClass('active');
		$('body').toggleClass('overflow-hidden');
	});

}

App.siteplanPopup = function () {

	$('.siteplan-popup').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		image: {
			verticalFit: true
		}

	});

}

App.galleryCarousel = function () {

	$('.carousel-1').slick({
	  centerMode: true,
	  centerPadding: '0',
	  slidesToShow: 1,
	  prevArrow: '<button type="button" class="slick-prev"></button>',
	  nextArrow: '<button type="button" class="slick-next"></button>'
	});

	$('.carousel-2').slick({
	  centerMode: true,
	  centerPadding: '0',
	  slidesToShow: 1,
	  prevArrow: '<button type="button" class="slick-prev"></button>',
	  nextArrow: '<button type="button" class="slick-next"></button>'
	});

	$('.carousel-3').slick({
	  centerMode: true,
	  centerPadding: '0',
	  slidesToShow: 1,
	  prevArrow: '<button type="button" class="slick-prev"></button>',
	  nextArrow: '<button type="button" class="slick-next"></button>'
	});

	$('.carousel-4').slick({
	  centerMode: true,
	  centerPadding: '0',
	  slidesToShow: 1,
	  prevArrow: '<button type="button" class="slick-prev"></button>',
	  nextArrow: '<button type="button" class="slick-next"></button>'
	});

	$('.slick-link').on('click', function(e) {
		e.preventDefault();
	});

}

App.homeMap = function () {

	$.getScript('https://www.google.com/jsapi', function() {
		google.load('maps', '3', { other_params: 'key=AIzaSyC13nd2Q0g_KPqAu_2X1PEz-jNV-sYalGM', callback: function() {

			var styles = [
				{
					stylers: [
						{ "saturation": -100 }
					]
				}, {
					featureType: "road.highway",
					stylers: [
						{ color: "#ffffff" }
					]
				}, {
					featureType: "administrative.neighborhood",
					stylers: [
						{ visibility: "off" }
					]
				},{
						featureType: "road",
						elementType: "labels",
						stylers: [
							{ visibility: "on" }
						]
					}
			];
			var mapOptions = {
				zoom: 15,
				scrollwheel: false,
				center: new google.maps.LatLng(33.776279, -84.382524),
				disableDefaultUI: true,
				styles: styles
			}
			var map = new google.maps.Map(document.getElementById('home-map'),
			                            mapOptions);

			var image = {url: '/wp-content/themes/midtown/images/map-icons/icon-home.png'};
			var myLatLng = new google.maps.LatLng(33.776279, -84.382524);
			var stetsonMarker = new google.maps.Marker({
			  position: myLatLng,
			  map: map,
			  icon: image
			});
		}
	}
)}
);
}

// Location Map
App.locationMap = function() {

	$.getScript('https://www.google.com/jsapi', function() {
		google.load('maps', '3', { other_params: 'key=AIzaSyC13nd2Q0g_KPqAu_2X1PEz-jNV-sYalGM', callback: function() {

			var styles = [
				{
					stylers: [
						{ "saturation": -100 }
					]
				}, {
					featureType: "road.highway",
					stylers: [
						{ color: "#ffffff" }
					]
				}, {
					featureType: "administrative.neighborhood",
					stylers: [
						{ visibility: "off" }
					]
				},{
						featureType: "road",
						elementType: "labels",
						stylers: [
							{ visibility: "on" }
						]
					}
			];
			var map_trigger = $('[data-location-id]'),
				filter_trigger = $('[data-category]'),
				filter_dropdown = $('#map-filter-dropdown'),
				map_options = {
					center: new google.maps.LatLng(33.776279, -84.382524),
					mapTypeControl: false,
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					navigationControl: false,
					navigationControlOptions: {
							style: google.maps.NavigationControlStyle.DEFAULT
					},
					scrollwheel: false,
					streetViewControl: false,
					zoom: 17,
					panControl: false,
					zoomControl: true,
					styles: styles,
					zoomControlOptions: {
						style: google.maps.ZoomControlStyle.LARGE,
						position: google.maps.ControlPosition.RIGHT_TOP
					}
				},
				map = new google.maps.Map(document.getElementById('neighborhood-map'), map_options),
				infowindow = new google.maps.InfoWindow(),
				markers = [],
				infowindows = [];

			map_trigger.click( function(e) {
			    e.preventDefault();

			    var location_id = parseInt( $(this).attr('data-location-id') );

			    infowindow.close();
			    infowindow.setContent( infowindows[location_id] );
			    infowindow.open( map, markers[location_id] );
			    map.panTo(markers[location_id].getPosition());
			    markers[location_id].setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
			  });

			  // Setting up the filter triggers
			filter_trigger.on('click', function(e) {
				e.preventDefault();

				var $locations = $('.locations-filters > li'),
					category = $(this).attr('data-category');

				filterLocations( category );

				// Change class on filters
				$(this).siblings().removeClass('is-active');
				$(this).addClass('is-active');

				// Change class on locations
				$locations.removeClass('is-active');
				$locations.parent().find('.map__locations-' + category).addClass('is-active');

			});

			// Filter the map markers
			function filterLocations(category) {
				var bounds = new google.maps.LatLngBounds(),
					i;

				// Defaults category to dining for manual trigger
				if ( !category ) {
					category = 'eat';
				}

				// Go through and hide/show markers while calculating the bounds
				for ( i = 0; i < markers.length; i++ ) {
					if ( markers[i] ) {
						if ( markers[i].category == 'home' ) {
							markers[i].setMap(map);
							bounds.extend(markers[i].getPosition());
						} else if ( markers[i].category !== category ) {
							markers[i].setMap(null);
						} else {
							markers[i].setMap(map);
							bounds.extend(markers[i].getPosition());
						}
					}
				}

				// Set map bounds
				map.setCenter(bounds.getCenter());
				map.fitBounds(bounds);

				// Remove one zoom level to ensure no marker is on the edge.
				// map.setZoom(map.getZoom()-1);
			};

			for ( var i = 0; i < AppData.locations.length; i += 1 ) {
				(function () {
					var location = AppData.locations[i],
						latitude = location.custom_fields._wpseo_coordinates_lat,
						longitude = location.custom_fields._wpseo_coordinates_long,
						latLng = new google.maps.LatLng(latitude, longitude),
						icon = { url: AppData.template_dir + '/images/map-icons/icon-' + location.category.slug + '.png' },
						marker = new google.maps.Marker({
							map: map,
							title: location.post_title,
							position: latLng,
							zIndex: 1,
							category: location.category.slug,
						}),
						content = [
							'<div class="infowindow">',
							'<strong>' + location.post_title + '</strong><br />',
							'<span>' + location.custom_fields._wpseo_business_address + '</span><br />',
							'<span>' + location.custom_fields._wpseo_business_city + ', ' + location.custom_fields._wpseo_business_state + ' ' + location.custom_fields._wpseo_business_zipcode + '</span>',
							'</div>'
						].join('');

					markers[location.ID] = marker;
					infowindows[location.ID] = content;

					marker.setIcon(icon);

					google.maps.event.addListener( marker, 'click', function() {
						infowindow.close();
						infowindow.setContent(content);
						infowindow.open(map, this);
						this.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
					});

					google.maps.event.addDomListener( window, 'resize', function() {
						var mapCenter = map.getCenter();
						google.maps.event.trigger(map, 'resize');
						map.setCenter(mapCenter);
					});
				}());
			}

			// Set dining as default filter
			var initialCategory = false;
			if ( initialCategory === false ) {
				$('.map__filter-businesses').trigger('click');
				initialCategory = true;
			}

		}});
	});
};

App.snippetsPopup = function () {


	$('.check').magnificPopup({
		items: {
			src: '#popper2',
			type: 'inline'
		},
		preloader: true
	});
	$('.remarketing').magnificPopup({
		items: {
			src: '#remarketing',
			type: 'inline'
		},
		preloader: true
	});
	$('.floorplan-popup').magnificPopup({
		type: 'ajax',
		mainClass: 'floorplan-modal',
		showCloseBtn: true,
		closeBtnInside: true
	});
}

App.shareThis = function () {

	var $sharethis = $('.st_sharethis');

	$sharethis.on('click', function(e) {
		e.preventDefault();
	});

	if (typeof window.stLight == 'undefined') {
		return;
	}

	window.stLight.options({
		version: '5x',
		publisher: '4d1be692-7a5f-4e5a-97a5-dbcebb190baa',
		excludeServices: 'linkedin',
		doNotHash: false,
		doNotCopy: true,
		hashAddressBar: false,
		onhover: false
	});
}

App.instagramFeed = function () {

	var feed = new Instafeed({
      get: 'user',
      userId: '4040024326', // Ex: 1374300081
      accessToken: '4040024326.3f5edaf.c8b980cbb9814524890326da3e44a3b6',
      template: '<li style="background-image: url({{image}})"><div class="overlay"><a href="{{link}}" class="insta-link">@BroadstoneMidtown</a><br /><span class="caption">{{caption}}</span></div></li>',
      limit: 6,
      resolution: 'standard_resolution'
    });
    feed.run();

}

App.fadeIn = function() {

	var controller = new ScrollMagic.Controller();

	var scene = new ScrollMagic.Scene({triggerElement: "#trigger-1"})
	// trigger animation by adding a css class
	.setClassToggle(".animate-1", "fade-in")
	.addTo(controller);

}

App.videoPopup = function () {

	$('.video-link').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		preloader: false,

		fixedContentPos: false
	});

}

App.virtualTour = function () {

	$('.tour').on('click', '.virtual-tour-link', function() {
        var $container = $(this).parent();

        event.preventDefault();

        $container.toggleClass('is-active');

        if ( $container.hasClass('is-active') ) {
            var video_id = $(this).data('video-id');
            var embed = '<iframe src="' + video_id + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'
            var iframe = $( embed );

            $container.find('.virtual-tour-link').append( iframe );
        } else {
            $(this).remove( iframe )
        }
    });

}

App.floorplanPopup = function () {

	$(".floorplans-link").click(function () {

        event.preventDefault();

    });

}

$(document).ready(function() {
    App.textWindow();
	App.shareThis();
	App.externalLink();
	App.menuToggle();
	//App.siteplanPopup();
	App.snippetsPopup();
	App.galleryCarousel();
	App.instagramFeed();
	App.fadeIn();
	App.videoPopup();
	App.virtualTour();

	if ( $('body').hasClass('home') ) {
		App.homeMap();
	}

	if ( $('body').hasClass('page-template-page-neighborhood-php') ) {
		App.locationMap();
	}
});
